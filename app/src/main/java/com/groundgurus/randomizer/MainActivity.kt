package com.groundgurus.randomizer

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.SeekBar
import android.widget.TextView
import android.widget.Toast
import java.util.*

class MainActivity : AppCompatActivity() {
    lateinit var rollButton: Button
    lateinit var seekBar: SeekBar
    lateinit var randomTextView: TextView
    lateinit var seekBarTextView: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initComponents()

        setSeekBarTextViewValue()

        initListeners()
    }

    private fun setSeekBarTextViewValue() {
        seekBarTextView.text = getString(R.string.seekBarTextViewText, seekBar.progress.toString())
    }

    private fun initComponents() {
        rollButton = findViewById(R.id.rollButton)
        seekBar = findViewById(R.id.seekBar)
        randomTextView = findViewById(R.id.randomTextView)
        seekBarTextView = findViewById(R.id.seekBarTextView)
    }

    private fun initListeners() {
        seekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {}

            override fun onStartTrackingTouch(seekBar: SeekBar?) {}

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
                setSeekBarTextViewValue()
            }
        })

        rollButton.setOnClickListener {
            if (seekBar.progress != 0) {
                val randomValue = Random().nextInt(seekBar.progress) + 1
                randomTextView.text = randomValue.toString()
            } else {
                randomTextView.text = getString(R.string.zeroValue)
                Toast.makeText(applicationContext, getString(R.string.seekBarZeroValueMsg), Toast.LENGTH_LONG)
                    .show()
            }
        }
    }
}
